import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;

public class EmailSender {

    public static void main(String[] args) {

        try {
            Email email = createEmail();
            email.setHostName("smtp.office365.com");
            email.setSmtpPort(587);

            email.setAuthentication("", "");
            email.setStartTLSEnabled(true);

            email.setFrom("");
            email.addTo("");

            email.setSubject("Helloeeee");
            email.setMsg("Helloeeee");

            email.send();

        } catch (EmailException e) {
            System.out.println(e.getMessage() + "\n\n");
            e.printStackTrace();
        }

    }

    protected static Email createEmail() {
        return new SimpleEmail();
    }
}
